import { apiService, IMoviesResponse } from './services/api';
import { favoriteService } from './services/favorite';
import { listUIService, favoriteUIService } from './services/ui';
import { MoviesHTMLListTabsTypes, IMainState } from './interfaces';
import { IMovie } from './common';
import { randomInteger } from './utils/randomUtils';

const mainState: IMainState = {
    page: 1,
    listTabType: null,
    searchValue: null,
};

const listTabsElement =
    document.querySelector<HTMLDivElement>('#button-wrapper');
const moviesContainerElement = listUIService.getContainerElement();
const loadMoreBtnElement = listUIService.getLoadMoreBtnElement();
const favoriteMoviesContainerElement = favoriteUIService.getContainerElement();
const searchBtnElement = document.querySelector<HTMLButtonElement>('#submit');
const searchInputElement = document.querySelector<HTMLInputElement>('#search');

const initialCheckedTabInputElement =
    listTabsElement?.querySelector<HTMLInputElement>(
        'input[type="radio"]:checked'
    ) ?? null;
const initialCheckedTabLabelElement =
    <HTMLLabelElement>initialCheckedTabInputElement?.nextElementSibling ?? null;

const randomMovieContainerElement =
    document.querySelector<HTMLDivElement>('#random-movie');
const randomMovieNameElement =
    document.querySelector<HTMLHeadingElement>('#random-movie-name');
const randomMovieDescriptionElement =
    document.querySelector<HTMLParagraphElement>('#random-movie-description');

function createEventListeners(): void {
    async function handleListTabsClick(e: MouseEvent): Promise<void> {
        if (!(<HTMLElement>e.target)?.classList.contains('btn-check')) {
            return;
        }

        const listTabType = <MoviesHTMLListTabsTypes | null>(
            (<HTMLElement>e.target).nextElementSibling?.getAttribute('for')
        );

        if (listTabType) {
            listUIService.reset();
            mainState.page = 1;
            mainState.searchValue = '';
            mainState.listTabType = listTabType;
            loadMovies();
        }
    }

    function handleLoadMoreClick(): void {
        mainState.page = mainState.page + 1;
        loadMovies();
    }

    async function handleSearchBtnClick(): Promise<void> {
        const searchValue = searchInputElement?.value;

        if (!searchInputElement || !searchValue) {
            return;
        }

        mainState.page = 1;
        mainState.searchValue = '';

        listUIService.reset();
        mainState.searchValue = searchValue;
        loadMovies();
    }

    function handleFavoriteIconClick(e: MouseEvent) {
        const heartIconElement = (<HTMLElement>e.target).closest(
            '.bi-heart-fill'
        );

        if (!heartIconElement) {
            return;
        }

        const movieId = Number(
            (<HTMLDivElement>heartIconElement.closest('[data-movie-id]'))
                ?.dataset.movieId ?? -1
        );

        if (movieId < 0) {
            return;
        }

        const isAddedToFavorites = favoriteService.checkExists(movieId);

        if (!isAddedToFavorites) {
            favoriteService.add(movieId);
            heartIconElement.classList.add('active');
        } else {
            favoriteService.remove(movieId);
            heartIconElement.classList.remove('active');
        }

        loadFavoriteMovies();
    }

    listTabsElement?.addEventListener('click', handleListTabsClick);
    loadMoreBtnElement?.addEventListener('click', handleLoadMoreClick);
    searchBtnElement?.addEventListener('click', handleSearchBtnClick);
    moviesContainerElement?.addEventListener('click', handleFavoriteIconClick);
    favoriteMoviesContainerElement?.addEventListener(
        'click',
        handleFavoriteIconClick
    );
}

async function loadMovies(): Promise<void> {
    const moviesResponse: IMoviesResponse = await (async () => {
        const { searchValue, listTabType, page } = mainState;

        if (searchValue) {
            return await apiService.searchMovies(searchValue, page);
        }

        switch (listTabType) {
            case MoviesHTMLListTabsTypes.TopRated: {
                return await apiService.getTopRatedMovies(page);
            }
            case MoviesHTMLListTabsTypes.Upcoming: {
                return await apiService.getUpcomingMovies(page);
            }
            default: {
                return await apiService.getPopularMovies(page);
            }
        }
    })();

    const { results: movies, totalResults: allMoviesCount } = moviesResponse;

    listUIService.renderMovies(movies);

    const renderedMoviesCount = listUIService.getRenderedMoviesCount();
    if (allMoviesCount === renderedMoviesCount) {
        listUIService.hideLoadMoreBtn();
    }

    if (mainState.page < 2) {
        loadRandomMovie(movies);
    }

    loadFavoriteMovies();
}

function loadRandomMovie(movies: IMovie[]): void {
    const randomMovieIndex = randomInteger(0, movies.length - 1);
    const {
        title: randomMovieTitle,
        overview: randomMovieOverview,
        posterPath: randomMoviePosterPath,
        backdropPath: randomMovieBackdropPath,
    } = movies[randomMovieIndex];

    if (randomMovieContainerElement) {
        randomMovieContainerElement.style.backgroundImage = `url(${
            randomMoviePosterPath || randomMovieBackdropPath
        })`;
    }
    if (randomMovieNameElement) {
        randomMovieNameElement.innerText = randomMovieTitle;
    }
    if (randomMovieDescriptionElement) {
        randomMovieDescriptionElement.innerText = randomMovieOverview;
    }
}

async function loadFavoriteMovies(): Promise<void> {
    const favoriteMovieIds = favoriteService.getAll();

    const favoriteMoviesResponse: IMovie[] = await apiService.getMoviesByIds(
        favoriteMovieIds
    );

    const moviesActiveHeartIconElements = document.querySelectorAll<SVGElement>(
        '.card .bi-heart-fill.active'
    );

    moviesActiveHeartIconElements.forEach((heartIconElement) => {
        heartIconElement.classList.remove('active');
    });

    favoriteMovieIds.forEach((favoriteMovieId) => {
        const heartIconElement =
            moviesContainerElement?.querySelector<SVGElement>(
                `[data-movie-id="${favoriteMovieId}"] .bi-heart-fill`
            );
        heartIconElement?.classList.add('active');
    });

    favoriteUIService.reset();
    favoriteUIService.renderMovies(favoriteMoviesResponse);
}

async function initialLoading(): Promise<void> {
    const initialListTabType = <MoviesHTMLListTabsTypes | null>(
        initialCheckedTabLabelElement?.getAttribute('for')
    );

    listUIService.reset();

    if (initialListTabType) {
        mainState.listTabType = initialListTabType;
        loadMovies();
    }
}

export async function render(): Promise<void> {
    initialLoading();
    createEventListeners();
}
