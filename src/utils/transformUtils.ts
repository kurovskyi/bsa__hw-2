export function humanizeEmptyValue<T>(value: T): string | T {
    if (!value) {
        return 'No information';
    }

    return value;
}
