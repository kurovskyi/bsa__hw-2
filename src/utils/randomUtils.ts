export function randomInteger(min: number, max: number): number {
    const randNum = min + Math.random() * (max + 1 - min);
    return Math.floor(randNum);
}
