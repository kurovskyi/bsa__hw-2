import { IMovie } from '../../common';
import { humanizeEmptyValue } from '../../utils';

class ListUIService {
    moviesContainerElement =
        document.querySelector<HTMLDivElement>('#film-container');
    loadMoreBtnElement =
        document.querySelector<HTMLButtonElement>('#load-more');

    getContainerElement(): HTMLDivElement | null {
        return this.moviesContainerElement;
    }

    getLoadMoreBtnElement(): HTMLButtonElement | null {
        return this.loadMoreBtnElement;
    }

    reset(): void {
        if (this.loadMoreBtnElement?.hidden) {
            this.loadMoreBtnElement.hidden = false;
        }
        if (this.moviesContainerElement?.innerHTML) {
            this.moviesContainerElement.innerHTML = '';
        }
    }

    renderMovies(movies: IMovie[]): void {
        let moviesFragment = '';
        movies.forEach((movie) => {
            moviesFragment += this.getMovieTemplate(movie);
        });

        console.log(movies.map((el) => el.overview));
        this.moviesContainerElement?.insertAdjacentHTML(
            'beforeend',
            moviesFragment
        );
    }

    private getMovieTemplate({
        id,
        posterPath,
        backdropPath,
        overview,
        releaseDate,
    }: IMovie): string {
        return `
          <div class="col-lg-3 col-md-4 col-12 p-2">
              <div class="card shadow-sm" data-movie-id="${id}">
                  <img
                      src="${posterPath || backdropPath}"
                  />
                  <svg
                      xmlns="http://www.w3.org/2000/svg"
                      stroke="red"
                      fill="#ff000078"
                      width="50"
                      height="50"
                      class="bi bi-heart-fill position-absolute p-2"
                      viewBox="0 -2 18 22"
                  >
                      <path
                          fill-rule="evenodd"
                          d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"
                      />
                  </svg>
                  <div class="card-body">
                      <p class="card-text truncate">
                          ${humanizeEmptyValue(overview)}
                      </p>
                      <div
                          class="
                              d-flex
                              justify-content-between
                              align-items-center
                          "
                      >
                          <small class="text-muted">${humanizeEmptyValue(
                              releaseDate
                          )}</small>
                      </div>
                  </div>
              </div>
          </div>
      `;
    }

    hideLoadMoreBtn(): void {
        if (this.loadMoreBtnElement?.hidden !== undefined) {
            this.loadMoreBtnElement.hidden = true;
        }
    }

    getRenderedMoviesCount(): number {
        return (
            this.moviesContainerElement?.querySelectorAll('.card').length ?? 0
        );
    }
}

export const listUIService = new ListUIService();
