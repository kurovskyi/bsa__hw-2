import { IMovie } from '../../common';

export interface IMoviesResponse {
    page: number;
    results: IMovie[];
    totalResults: number;
    totalPages: number;
}
