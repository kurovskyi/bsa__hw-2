import axios from 'axios';
import { camelizeKeys, decamelizeKeys, decamelize } from 'humps';

import { IMoviesResponse } from './interfaces';
import { IMovie } from '../../common';

class ApiService {
    private apiAxios = axios.create({
        baseURL: 'https://api.themoviedb.org/3',
        method: 'GET',
        params: {
            apiKey: '2779b61c33031c2e798cad976bcb61f2',
        },
    });

    constructor() {
        this.apiAxios.interceptors.request.use((config) => {
            config.params = decamelizeKeys(config.params);
            return config;
        });
        this.apiAxios.interceptors.response.use((response) => {
            response.data = camelizeKeys(response.data);
            return response;
        });
    }

    async getMoviesByIds(movieIds: number[]): Promise<IMovie[]> {
        const requestPromises: Promise<IMovie>[] = [];

        movieIds.forEach((movieId) => {
            requestPromises.push(this.getMovie(movieId));
        });

        return Promise.all(requestPromises);
    }

    async getAllMovies(
        type: 'popular' | 'topRated' | 'upcoming',
        page = 1
    ): Promise<IMoviesResponse> {
        const response = await this.apiAxios.request<IMoviesResponse>({
            url: `/movie/${decamelize(type)}`,
            params: {
                page,
            },
        });

        return {
            ...response.data,
            results: this.moviesMapper(response.data.results),
        };
    }

    async getPopularMovies(page = 1): Promise<IMoviesResponse> {
        return await this.getAllMovies('popular', page);
    }

    async getTopRatedMovies(page = 1): Promise<IMoviesResponse> {
        return await this.getAllMovies('topRated', page);
    }

    async getUpcomingMovies(page = 1): Promise<IMoviesResponse> {
        return await this.getAllMovies('upcoming', page);
    }

    async searchMovies(
        searchValue: string,
        page = 1
    ): Promise<IMoviesResponse> {
        const response = await this.apiAxios.request<IMoviesResponse>({
            url: `/search/movie`,
            params: {
                query: searchValue,
                page,
            },
        });

        return {
            ...response.data,
            results: this.moviesMapper(response.data.results),
        };
    }

    async getMovie(id: number): Promise<IMovie> {
        const response = await this.apiAxios.request<IMovie>({
            url: `/movie/${id}`,
        });

        return this.moviesMapper([response.data])[0];
    }

    private moviesMapper(moviesResponse: IMovie[]): IMovie[] {
        return moviesResponse.map(
            ({
                id,
                title,
                overview,
                posterPath,
                backdropPath,
                releaseDate,
            }) => ({
                id,
                title,
                overview,
                posterPath: posterPath
                    ? `https://image.tmdb.org/t/p/w500/${posterPath}`
                    : null,
                backdropPath: backdropPath
                    ? `https://image.tmdb.org/t/p/w500/${backdropPath}`
                    : null,
                releaseDate,
            })
        );
    }
}

export const apiService = new ApiService();
