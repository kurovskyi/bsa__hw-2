class FavoriteService {
    getAll(): number[] {
        const favoriteMovieIds = JSON.parse(
            localStorage.getItem('favoriteMovieIds') ?? '[]'
        );
        return favoriteMovieIds;
    }

    private getAllAsSet(): Set<number> {
        const favoriteMovieIdsSet = new Set<number>(this.getAll());
        return favoriteMovieIdsSet;
    }

    add(movieId: number): void {
        const favoriteMovieIdsSet = this.getAllAsSet();
        favoriteMovieIdsSet.add(movieId);
        localStorage.setItem(
            'favoriteMovieIds',
            JSON.stringify(Array.from(favoriteMovieIdsSet))
        );
    }

    remove(movieId: number): void {
        const favoriteMovieIdsSet = this.getAllAsSet();
        favoriteMovieIdsSet.delete(movieId);

        if (!favoriteMovieIdsSet.size) {
            return localStorage.removeItem('favoriteMovieIds');
        }
        localStorage.setItem(
            'favoriteMovieIds',
            JSON.stringify(Array.from(favoriteMovieIdsSet))
        );
    }

    checkExists(movieId: number): boolean {
        const favoriteMovieIdsSet = this.getAllAsSet();
        return favoriteMovieIdsSet.has(movieId);
    }
}

export const favoriteService = new FavoriteService();
