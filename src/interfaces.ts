export enum MoviesHTMLListTabsTypes {
    Popular = 'popular',
    TopRated = 'top_rated',
    Upcoming = 'upcoming',
}

export interface IMainState {
    page: number;
    listTabType: MoviesHTMLListTabsTypes | null;
    searchValue: string | null;
}
